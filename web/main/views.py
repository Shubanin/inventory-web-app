import django
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse


from .models import Object


#def index(request):
#    NAME_APP = "InventoryWebAPP732"
#    object_list = Object.objects.all()
#    return render(request,'main/index.html',{
#        "name": NAME_APP,
#        "object_list": object_list,
#    })


#def index():
#    object_list = request.GET.get('search', 'type')
#    return render(request,'main/index.html',{
#        "name": NAME_APP,
#        "object_list": object_list,
#    })



def index(request):
    NAME_APP = "InventoryWebAPP732"
    search_query = request.GET.get("search")
    search_type = request.GET.get("type")
    if search_query:
        if "object_name" == search_type:
            object_set = Object.objects.filter(object_name__icontains=search_query)
        elif "object_cabinet" == search_type:
            object_set = Object.objects.filter(object_cabinet__icontains=search_query)
        elif "object_people" == search_type:
            object_set = Object.objects.filter(object_people__icontains=search_query)
    else:
        object_set = Object.objects.all()
        return render(request, 'main/index.html', {
            "name": NAME_APP,
            "object_list": object_set,
        })
    return render(request, 'main/index.html', {
            "name": NAME_APP,
            "object_list": object_set,
        })
